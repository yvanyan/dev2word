# dev2word

#### 介绍
本项目着重各种开发文档生成为word文档, 用于项目交付使用, 节省时间, 目前已开发mysql数据库转word, swagger转word, 如有需要, 请自行下载

#### 软件架构
springboot框架

#### 安装教程

maven打包为jar包

#### 使用说明
1.  修改index.html中的script标签内的url改为自己将要部署的服务器IP,默认端口10010
2.  启动服务 java -jar dev2doc-0.0.1.jar
3.  访问服务 http://服务IP:10010/index.html
4.  按照页面的要求输入内容,点击导出即可在浏览器直接下载文档

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
