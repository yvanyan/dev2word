package com.yvan.dev2word.db2word.vo;

import lombok.Data;

@Data
public class TableDesc {
    private String Field;
    private String Type;
    private String Null;
    private String Key;
    private String Comment;
}
