package com.yvan.dev2word.db2word.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Table {
    private String tableName;
    private String tableComment;
    private String tableFullName;
    private List<TableDesc> descList = new ArrayList<>();
}
