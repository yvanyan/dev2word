package com.yvan.dev2word.swagger2doc.vo;

import lombok.Data;

import java.util.List;

@Data
public class ApiInterfaceVo {
    /**
     * 请求路径
     */
    private String uri;
    /**
     * 说明
     */
    private String explain;
    /**
     * 协议
     */
    private String agreement;
    /**
     * 请求json
     */
    private String requestJson;
    /**
     * 响应json
     */
    private String responseJson;
    /**
     * 响应json
     */
    private String tagName;
    /**
     * 请求formdata参数集合
     */
    private List<RequestBean> requestList;

    @Data
    public static class RequestBean {
        private String name;
        private String type;
        private Boolean required;
        private String description;
    }
}
